import * as csvParse from 'csv-parse';
import * as csvStringify from 'csv-stringify';
import * as fs from 'fs';
import { isObject } from 'util';

const RES_FILE_EXT: string = 'properties';
const RES_LOCALE_DEFAULT: string = 'default';

class ResEntry {
    public value: string;
    private _locale: string;
    private _key: string;

    constructor(key: string, value: string, locale: string = '') {
        this._key = key;
        this.value = value;

        if (!locale.length) {
            this._locale = RES_LOCALE_DEFAULT;
        } else {
            this._locale = locale;
        }
    }

    get locale() {
        return this._locale;
    }

    get key() {
        return this._key;
    }

    public toString() {
        return `${this._key}=${this.value}`;
    }
}

class ResFile {
    public static parseFile(fileName: string, mainFile: string, encoding: string = 'utf-8'): ResFile {
        const resFile = new ResFile(fileName);

        const fileNoExt = fileName.replace('.' + RES_FILE_EXT, '');
        let locale: string;

        if (fileNoExt === mainFile) {
            locale = RES_LOCALE_DEFAULT;
        } else {
            locale = fileNoExt.replace(mainFile + '_', '');
        }

        const file = fs.readFileSync(fileName, encoding);
        let matches;

        let entry: ResEntry;

        matches = this.reRes.exec(file);
        while (matches !== null) {
            entry = new ResEntry(matches[1], matches[2], locale);
            resFile.entries.push(entry);
            matches = this.reRes.exec(file);
        }

        return resFile;
    }

    public static getEntriesDiff(oldRes: ResFile, newRes: ResFile, originFile?: string): ResFile {
        const result = new ResFile(originFile);

        for (const entry of newRes.entries) {
            if (!oldRes.hasKey(entry.key)) {
                result.setEntry(entry);
            }
        }

        return result;
    }

    public static getUpdatedEntries(oldRes: ResFile, newRes: ResFile, originFile?: string): ResFile {
        const result = new ResFile(originFile);

        let newValue: string;
        for (const entry of oldRes.entries) {
            newValue = newRes.getValue(entry.key);

            if (entry.value !== newValue) {
                result.setEntry(new ResEntry(entry.key, newValue));
            }
        }

        return result;
    }

    private static readonly reRes: RegExp = /^([^=\n]+)=(.+)$/gm;

    public entries: ResEntry[];
    private _originFile: string;

    constructor(originFile?: string, items?: ResEntry[]) {
        if (items) {
            this.entries = items;
        } else {
            this.entries = [];
        }

        if (originFile) {
            this._originFile = originFile;
        } else {
            this._originFile = '';
        }
    }

    get originFile() {
        return this._originFile;
    }

    get length() {
        return this.entries.length;
    }

    get keys() {
        const result: string[] = [];

        for (const entry of this.entries) {
            result.push(entry.key);
        }

        return result;
    }

    public getValue(key: string) {
        const filtered = this.entries.filter( en => en.key === key);

        if (filtered.length === 1) {
            return filtered[0].value;
        } else if (filtered.length > 1) {
            return filtered[filtered.length - 1].value;
        }

        return '';
    }

    public hasKey(key: string): boolean {
        return !!this.entries.find((entry) => entry.key === key);
    }

    public remove(key: string) {
        this.entries = this.entries.filter((value) => value.key !== key);
    }

    public setEntry(entry: ResEntry) {
        const foundEntry = this.entries.find((existingEntry) => existingEntry.key === entry.key);

        if (!foundEntry) {
            this.entries.push(new ResEntry(entry.key, entry.value, entry.locale));
        } else {
            foundEntry.value = entry.value;
        }
    }

    public toString() {
        let result = '';

        for (const entry of this.entries) {
            result += entry.toString() + '\n';
        }

        return result;
    }
}

function createCsvFromRes(inputFileName: string) {
    const files: string[] = fs.readdirSync('./');
    const resFiles: string[] = [];
    const reAcceptedFiles = new RegExp(`^${inputFileName}(\\b|_.{1,5})\\.${RES_FILE_EXT}$`);

    for (const fileName of files) {
        if (reAcceptedFiles.test(fileName)) {
            resFiles.push(fileName);
        }
    }

    if (!resFiles.length) {
        console.log(`No properties files found starting with "${inputFileName}"`);
        return;
    }

    const parsedFiles = [];

    try {
        console.log('Reading input file: ' + inputFileName);
        for (const resFile of resFiles) {
            parsedFiles.push(ResFile.parseFile(resFile, inputFileName));
        }
    } catch (error) {
        console.error('❌ Error while reading input files!');
        console.error(error);
        return;
    }

    const headerColumns = ['key'];
    const allKeys: string[] = [];

    for (const parsedFile of parsedFiles) {
        headerColumns.push(parsedFile.originFile);
        allKeys.push(...parsedFile.keys);
    }

    const uniqueKeys = [...new Set(allKeys)];
    const csvData: string[][] = [];

    for (const key of uniqueKeys) {
        let csvRow: string[] = [];

        csvRow.push(key);

        for (const parsedFile of parsedFiles) {
            csvRow.push(parsedFile.getValue(key));
        }

        csvData.push(csvRow);
        csvRow = [];
    }

    try {
        csvStringify(csvData, { header: true, columns: headerColumns }, (error?: Error | null, output?: string) => {
            if (error) throw error;

            fs.writeFile(`${inputFileName}.csv`, output, (err: NodeJS.ErrnoException | null) => {
                if (err) throw err;
                console.log(`Success! "${inputFileName}.csv" created!`);
            });
        });
    } catch (error) {
        console.error('❌ Error while writing csv file!');
        console.error(error);
        return;
    }
}

function csvFileToResFileMap(csvFileContent: string): Map<string, ResFile> {
    const resFileList = new Map<string, ResFile>();

    const parser = csvParse(csvFileContent, {
        columns: true,
        skip_empty_lines: true,
    });

    let record = parser.read();

    if (!isObject(record)) {
        console.error('❌ CSV file is empty or corrupted!');
        return resFileList;
    }

    // Fill file list with the colum names:
    for (const columnTitle in record) {
        if (record.hasOwnProperty(columnTitle)) {
            if (typeof columnTitle === 'string' && columnTitle !== 'key') {
                resFileList.set(columnTitle, new ResFile(columnTitle));
            }
        }
    }

    do {
        for (const columnTitle in record) {
            if (record.hasOwnProperty(columnTitle)) {
                const value = record[columnTitle];

                if (typeof value === 'string' && value.length
                    && typeof record.key === 'string' && columnTitle !== 'key') {
                    const resFile = resFileList.get(columnTitle);
                    resFile?.setEntry(new ResEntry(record.key, value));
                }
            }
        }

        record = parser.read();
    } while (isObject(record));

    return resFileList;
}

function updateResFromCsv(inputFileName: string, deleteOldEntries?: boolean) {
    let csvFile: string;

    try {
        csvFile = fs.readFileSync(`./${inputFileName}.csv`, 'utf-8');
    } catch (error) {
        console.error('❌ Error while reading input file!');
        console.error(error);
        return;
    }

    const resFileList = csvFileToResFileMap(csvFile);

    let countDeleted = 0;
    let countUpdated = 0;
    let countNew = 0;

    for (const resFileEntry of resFileList) {
        let fileContents: string;
        let currentResFile: ResFile;

        if (fs.existsSync(resFileEntry[0])) {
            fileContents = fs.readFileSync(resFileEntry[0], 'utf-8');
            currentResFile = ResFile.parseFile(resFileEntry[0], inputFileName);
        } else {
            fileContents = '';
            currentResFile = new ResFile(resFileEntry[0]);
        }

        const updated = ResFile.getUpdatedEntries(currentResFile, resFileEntry[1]);
        const newEntries = ResFile.getEntriesDiff(currentResFile, resFileEntry[1]);

        if (deleteOldEntries) {
            const unused = ResFile.getEntriesDiff(resFileEntry[1], currentResFile);
            for (const unusedEntry of unused.entries) {
                const keyValueRE = new RegExp(`^${unusedEntry.key}=.*$`, 'gm');
                fileContents = fileContents.replace(keyValueRE, '');
                updated.remove(unusedEntry.key);
                countDeleted++;
            }
        }

        for (const updatedEntry of updated.entries) {
            const keyValueRE = new RegExp(`^${updatedEntry.key}=.*$`, 'gm');
            fileContents = fileContents.replace(keyValueRE, updatedEntry.toString());
            countUpdated++;
        }

        if (newEntries.length) {
            fileContents += '\n' + newEntries.toString();
            countNew += newEntries.length;
        }

        fs.writeFileSync(resFileEntry[0], fileContents);
    }

    if (countDeleted) {
        console.log(`Deleted ${countDeleted} entries.`);
    }

    if (countUpdated) {
        console.log(`Updated ${countUpdated} entries.`);
    }

    if (countNew) {
        console.log(`Added ${countNew} new entries.`);
    }
}

function execute() {
    if (process.argv.length === 4 && process.argv[2] === 'c') {
        createCsvFromRes(process.argv[3]);
    } else if (process.argv.length === 4 && process.argv[2] === 's') {
        updateResFromCsv(process.argv[3]);
    } else if (process.argv.length === 4 && process.argv[2] === 'sd') {
        updateResFromCsv(process.argv[3], true);
    } else {
        console.log('❌ Invalid arguments!');
        console.log(`
Example: "node index.js c account"
to create csv file from resource files
OR
Example: "node index.js s account"
to update resource files based on a csv file
OR
Example: "node index.js sd account"
to update and delete resource files based on a csv file`);
    }
}

console.time('⏱');
execute();
console.timeEnd('⏱');
console.log('✔💯');
