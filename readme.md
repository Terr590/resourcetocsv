# ResourceToCSV
## Set up:
### Installation:
```Bash
npm i
npm run build
```
### Adding it as command to path:
1. Get absolute path of `bin/index.js` (e.g. `C:\Projects\nodejs_rescsv\bin\index.js`)
2. Add an alias for it in bash:
```Bash
alias rescsv='node "C:\Projects\nodejs_rescsv\bin\index.js"'
```

## Usage:
1. Open bash
2. Navigate to resource
3. To generate csv form resource files use:
```Bash
rescsv c resourcefilename
```
Example (using account.properties, account_de.properties, etc..):
```Bash
rescsv c account
```

4. To update resource files from csv use:
```Bash
rescsv s account
```

## For debugging in VS Code add the folowing files:
`.vscode/tasks.json`
```JSON
{
    "version": "2.0.0",
    "tasks": [{
        "type": "typescript",
        "tsconfig": "tsconfig.json",
        "problemMatcher": [
            "$tsc"
        ]
    }]
}
```
`.vscode/launch.json`
```JSON
{
    "version": "0.2.0",
    "configurations": [{
        "type": "node",
        "request": "launch",
        "name": "Debug Create",
        "preLaunchTask": "npm: build",
        "program": "${workspaceFolder}\\index.ts",
        "args": ["c", "account"],
        "cwd": "${workspaceFolder}",
        "protocol": "inspector",
        "outFiles": [
            "${workspaceFolder}/bin/**/*.js"
        ]
    },{
        "type": "node",
        "request": "launch",
        "name": "Debug Save",
        "preLaunchTask": "npm: build",
        "program": "${workspaceFolder}\\index.ts",
        "args": ["s", "account"],
        "cwd": "${workspaceFolder}",
        "protocol": "inspector",
        "outFiles": [
            "${workspaceFolder}/bin/**/*.js"
        ]
    }]
}
```